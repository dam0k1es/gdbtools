#!/usr/bin/env python3

import gdb

import re
import argparse

from tabulate import tabulate


class DumpTable(gdb.Command):
    '''
    dt (dump table)
    Dump a table containing information about specific data related to stack frames.
    If specified, a stack frame is selected as it is numbered in backtrace, for example.

    dt s|stack:         Dump the name of all the previous stackframes
    dt r|registers:     Dump information about the registers
    dt c|call  [n]:     Dump the name of the function, it's parrent and child
    dt ''|f|frame [n]:  Dump a stackframe
    '''

    def __init__(self):
        super(DumpTable, self).__init__("dt", gdb.COMMAND_USER)

    def get_frame_by_nr(self, nr: int) -> gdb.Frame:
        frame = gdb.newest_frame()
        while frame and nr > 0:
            nr -= 1
            frame = frame.older()

        return frame

    def info_stack(self):
        frame = gdb.newest_frame()
        frames = []
        index = 0
        while frame:
            frames.append([f"Frame {index}", frame.name()])
            index += 1
            frame = frame.older()

        print(tabulate(frames, headers=[
              "Name", "Value"], tablefmt="grid") + "\n")

    def info_registers(self):
        frame = gdb.selected_frame()
        registers = []
        abi_regs = ["rdi", "rsi", "rdx", "rcx", "r8", "r9"]
        pointer_regs = ["rbp", "rsp"]
        regs = abi_regs + pointer_regs

        for reg in regs:
            if (val := str(frame.read_register(reg))):
                registers.append([reg, val])

        print(tabulate(registers,
                       headers=["Name", "Value"],
                       tablefmt="grid") + "\n")

    def info_call(self, nr=0) -> bool:
        frame = self.get_frame_by_nr(nr)
        meta = []

        if (fname := frame.name()) != None:
            meta.append(["Function", fname])
        else:
            print("Fatal error: No function name found")
            return False
        if (parent_frame := frame.older()) != None:
            meta.append(["Parent", str(parent_frame.name())])
        if (child_frame := frame.newer()) != None:
            meta.append(["Child", str(child_frame.name())])

        print(tabulate(meta,
                       headers=["Name", "Value"],
                       tablefmt="grid") + "\n")
        return True

    def info_frame(self, nr=0) -> bool:
        content = []
        frame = self.get_frame_by_nr(nr)

        if (rbp_addr := str(frame.read_register('rbp'))):
            if rbp_addr == "0x1":
                print("Fatal error: Base Pointer is malformed")
                return False
            if (saved_rsp := gdb.execute("x/x"+rbp_addr+"+0x8", to_string=True)):
                saved_rsp = hex(int(saved_rsp.split(":")[1], 16))
                saved_rsp_addr = hex(int(rbp_addr, 16)+int('0x8', 16))
                content.append([saved_rsp_addr, "Return value", saved_rsp])
            if (saved_rbp := gdb.execute("x/g"+rbp_addr, to_string=True)):
                saved_rbp = hex(int(saved_rbp.split(":")[1], 16))
                content.append([rbp_addr, "Saved RBP", saved_rbp])
        else:
            print("Fatal error: Base Pointer not found")

        gdb.execute("select-frame " + str(nr))

        locals = gdb.execute("info locals", to_string=True)
        if locals != "No locals.\n" and locals is not None:
            locals = locals.split("\n")
            for index, localv in enumerate(locals):
                local = str(localv.split(" = ")[0])
                if local is not None and local != "":
                    val = str(gdb.execute("print " + local,
                              to_string=True)).split(" = ")[1]
                    addr = re.split(
                        r'\(.*\)', str(gdb.execute("print &" + local, to_string=True)))[1]
                    content.append([addr, "Local " + str(index), val])

        args = gdb.execute("info args", to_string=True)
        if args != "No arguments.\n" and args is not None:
            args = args.split("\n")
            for index, argv in enumerate(args):
                arg = str(argv.split(" = ")[0])
                if arg is not None and arg != "":
                    val = str(gdb.execute("print " + arg,
                              to_string=True)).split(" = ")[1]
                    addr = re.split(
                        r'\(.*\)', str(gdb.execute("print &" + arg, to_string=True)))[1]
                    content.append([addr, "Argument " + str(index), val])

        print(tabulate(content,
                       headers=["Adress", "Name", "Value"],
                       tablefmt="grid") + "\n")
        return True

    def parse_args(self, args: str):
        match args[0]:
            case 's' | 'stack':
                self.info_stack()
            case 'r' | 'registers':
                self.info_registers()
            case 'c' | 'call':
                self.info_call() if len(
                    args) == 1 else self.info_call(int(args[1]))
            case '' | 'f' | 'frame':
                self.info_frame() if len(
                    args) == 1 else self.info_frame(int(args[1]))
            case _:
                pass

    def invoke(self, arg, from_tty):
        try:
            gdb.selected_frame()
            self.parse_args(arg.split(" "))
            return True
        except RuntimeError as e:
            print(f"Error {e}: Can not select frame")
            return False


DumpTable()
