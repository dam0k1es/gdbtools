# Sets the notation for the disassembly display
set disassembly-flavour intel

# GDB does not attempt to query debuginfod server if
# Debug information or source files are missing.
set debuginfod enabled off

# Activates the "nicely formatted" output of
# data structures such as arrays and objects.
set print pretty on

# Define your own - more visible - prompt (newline against ANSI errors)               
set prompt \033[31mGDB:\033[0m\n

# Activates the saving of the command history in a
# file so that it is available again at the next start.
set history save on

# Specifies the name of the file in which the
# command history is to be saved.
set history filename ~/.gdb_history

# Activates the logging of GDB outputs.
set logging enabled on

# Activates the logging of GDB outputs to a file.
set logging file ~/.gdb_log
