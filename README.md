# gdbtools

## About

The repository contains tools for understanding and working with the GNU
Debugger. At the moment it consists of a project that aims to familiarise
students with stack frames/function calls in x86 assembler. The dotfiles for GDB
are also included.

## Usage example
```sh
gdb: source dump_frame.py
gdb: break func1
gdb: dt stack 
gdb: dt registers
gtd: dt call
gtb: dt frame 
gtb: dt frame 1
```
