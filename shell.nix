with import <nixpkgs> {};

let
   python = enableDebugging pkgs.python;
in
  stdenv.mkDerivation {
    name = "test";
    buildInputs = [ 
	python311 
	python311Packages.tabulate 
  ];
}
